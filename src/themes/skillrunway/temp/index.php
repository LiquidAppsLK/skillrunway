<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/assets/dist/css/app.css">

</head>

<body class="bg-gray-100 ">
    <section class="h-screen p-8">
        <div class="flex items-center h-full p-24 bg-red-100 rounded-3xl">
            <div class="w-full max-w-screen-xl mx-auto ">
                <div class="w-1/3 ">
                    <h1 class="text-5xl font-bold leading-none">
                        Start Learning
                        <span class="block ">
                            New Skills
                        </span>
                    </h1>
                    <p class="mt-6 mb-12 ">Learn almost any skill from a comfort <br> of your home with Skill Runway.</p>
                    <a href="" class="px-6 py-4 text-white bg-pink-500 rounded-md">Create Your Free Account</a>
                </div>
            </div>
        </div>
    </section>
    <section class="p-8">
        <div class="max-w-screen-xl mx-auto ">
            <div class="flex">
                <div class="w-4/6 ">
                    <div class="p-8 mr-12 bg-white rounded-3xl">
                        <h3 class="text-sm font-semibold uppercase">Editor’s pick</h3>
                        <div class="mt-6 space-y-6 ">
                            <?php foreach ([1, 2, 3] as $item) : ?>
                                <?php get_template_part('components/card', 'medium') ?>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
                <div class="w-2/6 ">
                    <div class="p-8 mr-12 bg-white rounded-3xl">
                        <h3 class="text-sm font-semibold uppercase">top catgeories</h3>
                        <div class="mt-6 space-y-8 ">
                            <?php foreach ([1, 2, 3, 4] as $item) : ?>
                                <div class="flex items-center ">
                                    <div class="w-12 h-12 bg-gray-400 rounded-xl"></div>
                                    <div class="ml-4 ">
                                        <h5 class="text-sm font-semibold">
                                            Freelancing
                                        </h5>
                                        <p class="mt-1 text-xs">
                                            24 Articles | 08 Podcast
                                        </p>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="p-8">
        <div class="max-w-screen-xl mx-auto ">
            <div class="flex">
                <div class="w-full ">
                    <div class="p-8 mr-12 bg-white rounded-3xl">
                        <h3 class="text-sm font-semibold uppercase">Trending on Skill Runway</h3>
                        <div class="mt-6">
                            <div class="grid grid-cols-3 gap-x-4 gap-y-12">
                                <?php foreach ([1, 2, 3, 4, 5, 6, 7, 8, 9] as $item) : ?>
                                    <?php get_template_part('components/card', 'small') ?>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="p-8">
        <div class="max-w-screen-xl mx-auto ">
            <div class="flex">
                <div class="w-4/6 ">
                    <?php foreach ([1, 2, 3, 4, 5, 6, 7, 8, 9] as $item) : ?>
                        <?php get_template_part('components/card', 'large') ?>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </section>
    <script src="<?php echo get_template_directory_uri() ?>/assets/dist/js/app.js"></script>
</body>

</html>






<div class="search__wrap">
    <form action="" class=" d-flex align-items-center">
        <div class="search__wrap--input">
            <input type="text" name="" placeholder="Search your favorite skill">
        </div>
        <div class="search__wrap--button">
            <button type="submit">
                <svg viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M19.756 18.607l-3.438-3.361-.08-.123a.806.806 0 00-1.137 0c-2.921 2.68-7.423 2.826-10.519.34C1.486 12.979.756 8.634 2.876 5.31 4.996 1.987 9.308.717 12.953 2.342c3.645 1.625 5.49 5.642 4.314 9.386a.78.78 0 00.182.771.821.821 0 00.774.232.803.803 0 00.593-.54c1.406-4.442-.718-9.223-5-11.25C9.534-1.085 4.381.251 1.69 4.085-1.003 7.92-.425 13.102 3.05 16.28c3.474 3.178 8.8 3.397 12.535.516l3.044 2.975a.819.819 0 001.137 0 .784.784 0 000-1.12l-.01-.043z" fill="#fff" />
                </svg>
            </button>
        </div>
    </form>
</div>
<a href="#">
    <span>Create Your Free Account</span>
</a>