<div class="px-8 py-6 mb-10 bg-white rounded-3xl">
    <div class="flex">
        <div class="bg-gray-400 rounded-md h-7 w-7"></div>
        <div class="ml-4 ">
            <p class="mt-1 text-xs">
                Featured in 
                <a href="">Freelacing</a>
            </p>
            <h5 class="text-lg font-semibold">
                An Artist Created Lifelike Photos of the Wives of King Henry VIII
            </h5>
            <p class="my-3 text-xs">
                10 critical usability issues and practical recommendations on to avoid when working on your mobile design
            </p>
            <p class="mt-1 text-xs">
                <span>Dec 7  ~  5 min read</span>
            </p>
        </div>
        <div class="h-32 bg-gray-400 rounded-md w-44"></div>
    </div>
</div>