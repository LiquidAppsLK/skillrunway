<div class="flex items-center ">
    <div class="w-10 h-10 bg-gray-400 rounded-xl"></div>
    <div class="ml-4 " style="width: calc(100% - 2.5rem)">
        <h5 class="text-xs font-semibold">
            An Artist Created Lifelike Photos of the Wives of King Henry VIII
        </h5>
        <p class="mt-1 text-xs">
            Featured in 
            <a href="">Freelacing</a>
            <span>Dec 7  ~  5 min read</span>
        </p>
    </div>
</div>