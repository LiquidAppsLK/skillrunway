const mix = require('laravel-mix');

mix.js('assets/js/app.js', 'assets/dist/js/')
   .sass('assets/scss/app.scss', 'assets/dist/css/')
   .options({
        processCssUrls: false,
    });