<?php get_header(); ?>

<section class="cover--other__wrap">
    <div class="container">
        <div class="cover--other__wrap--inner d-flex align-items-center">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-10 col-lg-8 ">
                        <div class="post--title">
                            <h1>
                                <?php echo get_the_author(); ?>
                                <?php
                                $author_id = get_the_author_meta('ID');
                                ?>
                            </h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="content__wrap">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-xl-8 col-xxl-9">
                <div class="card transparent">
                    <div class="card__title">
                        <h4>Trending</h4>
                    </div>

                    <?php
                    $trendingposts = get_posts(array(
                        'posts_per_page' => 9,
                        'author'       => $author_id,
                    ));
                    $trendingchunk = array_chunk($trendingposts, 3, true);
                    ?>
                    <div class="card__body">
                        <div class="row">
                            <div class="col-md-4 col-lg-4">
                                <?php
                                if ($trendingchunk[0]) :
                                    foreach ($trendingchunk[0] as $post) :
                                ?>
                                        <?php get_template_part('components/article', 'card', array('type' => 'small', 'post' => [])) ?>
                                <?php
                                    endforeach;
                                    wp_reset_postdata();
                                endif;
                                ?>
                            </div>
                            <div class="col-md-4 col-lg-4">
                                <?php
                                if ($trendingchunk[1]) :
                                    foreach ($trendingchunk[1] as $post) :
                                ?>
                                        <?php get_template_part('components/article', 'card', array('type' => 'small', 'post' => [])) ?>
                                <?php
                                    endforeach;
                                    wp_reset_postdata();
                                endif;
                                ?>
                            </div>
                            <div class="col-md-4 col-lg-4">
                                <?php
                                if ($trendingchunk[2]) :
                                    foreach ($trendingchunk[2] as $post) :
                                ?>
                                        <?php get_template_part('components/article', 'card', array('type' => 'small', 'post' => [])) ?>
                                <?php
                                    endforeach;
                                    wp_reset_postdata();
                                endif;
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-xl-4 col-xxl-3">
                <div class="card">
                    <div class="card__title">
                        <h4>top catgeories</h4>
                    </div>
                    <div class="card__body">
                        <?php
                        $categories = get_categories();
                        foreach ($categories as $key => $category) :
                        ?>
                            <div class="catgeory">
                                <div class="catgeory__icon">
                                    <div class="catgeory__icon--inner"></div>
                                </div>
                                <div class="catgeory__content">
                                    <h4> <a href="<?php echo get_category_link($category->term_id) ?>"> <?php echo $category->name; ?> </a> </h4>
                                    <p>
                                        24 Articles | 08 Podcast
                                    </p>
                                </div>
                            </div>
                        <?php if ($key == 3) : break;
                            endif;
                        endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="content__wrap">
    <div class="container">
        <div class="row">
            <div class="col-lg-5">
                <div class="card transparent">
                    <div class="card__title">
                        <h4>Latest Articles</h4>
                    </div>
                    <div class="card__body">
                        <?php
                        $latesposts = get_posts(array(
                            'posts_per_page' => 4,
                            'author'       => $author_id,
                        ));

                        if ($latesposts) :
                            foreach ($latesposts as $post) :
                        ?>
                                <?php get_template_part('components/article', 'card', array('type' => 'large', 'post' => [])) ?>
                        <?php
                            endforeach;
                            wp_reset_postdata();
                        endif;
                        ?>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="card transparent">
                    <div class="card__title">
                        <h4>Editor’s pick</h4>
                    </div>
                    <div class="card__body">
                        <?php
                        $editorposts = get_posts(array(
                            'posts_per_page' => 6,
                            'author'       => $author_id,
                        ));

                        if ($editorposts) :
                            foreach ($editorposts as $post) :
                        ?>
                                <?php get_template_part('components/article', 'card', array('type' => 'medium', 'post' => [])) ?>
                        <?php
                            endforeach;
                            wp_reset_postdata();
                        endif;
                        ?>
                    </div>
                </div>
            </div>
            <div class="col-lg-3"></div>
        </div>
    </div>
</section>
<?php get_footer(); ?>