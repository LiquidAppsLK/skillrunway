<!DOCTYPE html>
<html lang="en">

<head <?php language_attributes(); ?> class="no-js">
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title><?php wp_title(''); ?><?php if (wp_title('', false)) {
                                        echo ' :';
                                    } ?> <?php bloginfo('name'); ?></title>
    <link href="//www.google-analytics.com" rel="dns-prefetch">

    <!-- <link rel="shortcut icon" href="<?php //echo get_template_directory_uri() 
                                            ?>/assets/images/favicon/favicon.ico">
	<link rel="icon" href="<?php //echo get_template_directory_uri() 
                            ?>/assets/images/favicon/favicon-16x16.png" type="image/png" sizes="16x16">
	<link rel="icon" href="<?php //echo get_template_directory_uri() 
                            ?>/assets/images/favicon/favicon-32x32.png" type="image/png" sizes="32x32">

	<link rel="apple-touch-icon" sizes="180x180" href="<?php //echo get_template_directory_uri() 
                                                        ?>/assets/images/favicon/apple-touch-icon.png">
	<link rel="apple-touch-icon" sizes="192x192" href="<?php //echo get_template_directory_uri() 
                                                        ?>/assets/images/favicon/android-chrome-192x192.png">
	<link rel="apple-touch-icon" sizes="512x512" href="<?php //echo get_template_directory_uri() 
                                                        ?>/assets/images/favicon/android-chrome-512x512.png"> -->

    <meta name="description" content="<?php bloginfo('description') ?>">
    <meta name="keywords" content="Blog">
    <meta name="author" content="TWA">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->

    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Gabriela&family=Inter:wght@500&family=Poppins:wght@300;400;600;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/assets/dist/css/app.css">

</head>

<body>
    <div class="wrapper">
        <?php if (is_active_sidebar('language')) : ?>
            <?php dynamic_sidebar('language'); ?>
        <?php endif; ?>
        <header class="header__wrap">
            <div class="d-flex justify-content-between align-items-center">
                <div class="logo__wrap">
                    <a href="<?php echo home_url(); ?>">
                        <svg width="155" height="55" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" clip-rule="evenodd" d="M16 0h23c8.837 0 16 7.164 16 16v23c0 8.836-7.163 16-16 16H16C7.163 55 0 47.836 0 39V16C0 7.164 7.163 0 16 0zm51.53 30.79c1.6.42 3.25.63 4.95.63 2 0 3.7-.3 5.1-.9 1.4-.62 2.45-1.44 3.15-2.46.7-1.02 1.05-2.16 1.05-3.42 0-1.46-.39-2.61-1.17-3.45a6.547 6.547 0 00-2.73-1.86c-1.04-.4-2.36-.77-3.96-1.11-1.42-.3-2.45-.59-3.09-.87-.64-.28-.96-.7-.96-1.26s.27-1.01.81-1.35c.54-.36 1.4-.54 2.58-.54.948 0 1.91.137 2.89.412.962.27 1.939.673 2.93 1.208l1.83-4.41c-1.02-.6-2.2-1.05-3.54-1.35-1.34-.32-2.7-.48-4.08-.48-2.02 0-3.73.3-5.13.9-.756.33-1.41.724-1.963 1.185-.455.38-.84.805-1.157 1.275-.7 1.02-1.05 2.17-1.05 3.45 0 1.46.38 2.62 1.14 3.48.76.86 1.67 1.49 2.73 1.89 1.06.38 2.38.74 3.96 1.08 1.44.32 2.48.63 3.12.93.66.28.99.72.99 1.32 0 1.14-1.14 1.71-3.42 1.71-1.2 0-2.42-.19-3.66-.57-1.24-.38-2.33-.88-3.27-1.5l-1.95 4.38c1 .7 2.3 1.26 3.9 1.68zm22.873-5.04l2.221-2.37 6 7.62h6.899l-9.029-11.7 8.58-9.3h-6.54l-8.13 8.73V10h-5.88v21h5.88v-5.25zM113.021 10h-5.939v21h5.939V10zm10.167 0h-5.94v21h15.96v-4.71h-10.02V10zm18.311 0h-5.94v21h15.96v-4.71h-10.02V10zM70.771 43.105L73.487 47h2.66l-3.06-4.37c.913-.355 1.61-.893 2.09-1.615.495-.722.742-1.583.742-2.584 0-.454-.05-.88-.148-1.277a4.158 4.158 0 00-.536-1.25c-.456-.71-1.115-1.254-1.976-1.634-.849-.38-1.85-.57-3.002-.57h-5.472V47h2.47v-3.876h3.002c.228 0 .4-.006.513-.019zm1.824-6.63c.557.455.836 1.108.836 1.956 0 .849-.279 1.508-.836 1.976-.558.456-1.374.684-2.451.684h-2.888V35.79h2.888c1.077 0 1.893.228 2.45.684zm7.513 9.176c1.027 1.026 2.457 1.539 4.294 1.539 1.837 0 3.262-.513 4.276-1.54 1.025-1.038 1.539-2.52 1.539-4.445V33.7h-2.433v7.41c0 2.61-1.12 3.914-3.363 3.914-2.254 0-3.382-1.305-3.382-3.914V33.7h-2.47v7.505c0 1.925.513 3.407 1.54 4.446zM105.494 47V33.7h-2.451v9.006L95.708 33.7h-2.032V47h2.451v-9.006L103.461 47h2.033zm18.5 0l4.408-13.3h-2.356l-3.401 10.165-3.363-10.165h-2.28l-3.44 10.108-3.305-10.108h-2.566L112.1 47h2.641l3.344-9.842L121.372 47h2.622zm6.658 0l1.311-3.078h6.65l1.33 3.078h2.584l-6.005-13.3h-2.431L128.105 47h2.547zm4.636-10.83l2.508 5.814h-4.997l2.489-5.814zM149.159 47v-4.693l5.206-8.607h-2.432L148 40.18l-3.896-6.48h-2.64l5.224 8.645V47h2.471z" fill="#000" />
                            <path d="M41 8v7.667c0 1.906-1.595 3.679-4.182 4.653l-.173.045-4.915 1.773L19.44 18.99a3.085 3.085 0 01-1.25-.665c.258.266.603.488 1.035.665l17.376 6.249c.13.044.216.088.345.133-.043-.045-.13-.045-.172-.089l-.087-.044h.043c2.544.93 4.14 2.703 4.226 4.565v4.697c0 1.906-1.596 3.679-4.182 4.654l-.173.044L15 47v-7.667c0-1.906 1.595-3.678 4.182-4.654l.173-.044 17.333-6.249c.431-.177.82-.398 1.035-.664l-.043.044c-.13.044-.302.133-.431.177L23.925 31.49l-4.613-1.684C16.638 28.873 15 27.056 15 25.107v-4.565c0-1.684 1.207-3.235 3.19-4.255l.044-.044.043-.044.172-.045.647-.265.216-.089L41 8z" fill="#fff" />
                        </svg>
                    </a>
                </div>
                <div class="navigation__wrap">
                    <nav class="navigation">
                        <?php if (is_user_logged_in()) : ?>
                            <ul>
                                <li><a href="">Bookmark</a></li>
                                <li><a href="">My Account</a></li>
                            </ul>
                        <?php else : ?>
                            <ul>
                                <li><a href="">Login</a></li>
                                <li><a href="">Create Free Account</a></li>
                            </ul>
                        <?php endif; ?>
                    </nav>
                </div>
            </div>
        </header>