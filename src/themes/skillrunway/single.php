<?php

get_header();

if (get_the_post_thumbnail_url($post->ID, 'post-thumbnail')) {
    $cover_image  =  get_the_post_thumbnail_url($post->ID, 'post-thumbnail');
} else {
    $cover_image  =  '';
}


?>

<?php while (have_posts()) : the_post(); ?>
    <section class="cover--other__wrap">
        <div class="container">
            <div class="cover--other__wrap--inner --post d-flex align-items-center" style="background-image: url('<?php echo $cover_image; ?>');">
                <div class="row justify-content-center">
                    <div class="col-md-10 col-lg-8 ">
                        <div class="post--title">
                            <h1>
                                <?php the_title(); ?>
                            </h1>
                        </div>
                        <div class="post--category">
                            <?php
                            $categories = get_the_category();
                            ?>
                            <p>
                                Featured in
                                <a href=""><?php echo category($categories) ?></a>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="post--meta">
                    <div class="container">
                        <div class="d-flex justify-content-between align-items-center">
                            <?php
                            $author_id = get_the_author_meta('ID');
                            ?>
                            <div class="author">
                                <div class="author__image">
                                    <div class="author__image--inner"></div>
                                </div>
                                <div class="author__content">
                                    <div class="d-flex align-items-center">
                                        <div class="w-100">
                                            <h4>
                                                <?php echo get_the_author_meta('display_name', $author_id); ?>
                                                <a href="<?php echo get_author_posts_url($author_id) ?>" class="author__content--btn">
                                                    Follow
                                                </a>
                                            </h4>
                                            <p>
                                                Post on <?php echo get_the_date('F j, Y'); ?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="content__wrap">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-10 col-lg-8 ">
                    <div class="content__wrap--post__content">
                        <?php the_content(); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="content__wrap">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-10 col-lg-8 ">
                    <?php
                    $posttags = get_the_tags();
                    if ($posttags) :
                    ?>
                        <div class="content__wrap--tag__content">
                            <ul>
                                <?php
                                foreach ($posttags as $tag) :
                                ?>
                                    <li><a href="#"><?php echo $tag->name; ?></a></li>
                                <?php
                                endforeach;
                                ?>
                            </ul>
                        </div>
                    <?php
                    endif;
                    ?>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-10 col-lg-8 ">
                    <div class="content__wrap--respond__content">

                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endwhile; ?>
<section class="content__wrap">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10 col-lg-8 ">
                <div class="content__wrap--follow__content">
                    <?php
                    $author_id = get_the_author_meta('ID');
                    ?>
                    <div class="follow">
                        <div class="follow__image">
                            <div class="follow__image--inner"></div>
                        </div>
                        <div class="follow__content">
                            <div class="d-flex align-items-center">
                                <div class="w-100">
                                    <p>
                                        <span>WRITTEN BY</span>
                                    </p>
                                    <h4> <?php echo get_the_author_meta('display_name', $author_id); ?> </h4>
                                    <p>
                                        <?php echo get_the_author_meta('description', $author_id); ?>
                                    </p>
                                </div>
                                <div>
                                    <a href="<?php echo get_author_posts_url($author_id) ?>" class="follow__content--btn">
                                        Follow
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
<section class="content__wrap">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10 col-lg-8 ">
                <?php
                $relatedposts = get_posts(array(
                    'posts_per_page' => 3,
                    'post__not_in' => array(get_the_ID()),
                ));

                if ($relatedposts) :
                    foreach ($relatedposts as $post) :
                ?>
                        <div class="card card--margin__bottom">
                            <div class="card__body">
                                <?php get_template_part('components/article', 'card', array('type' => 'large', 'post' => $post)) ?>
                            </div>
                        </div>
                <?php
                    endforeach;
                    wp_reset_postdata();
                endif;
                ?>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>