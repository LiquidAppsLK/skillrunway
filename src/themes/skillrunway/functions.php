<?php

add_theme_support('wp-block-styles');
add_theme_support('align-wide');

remove_filter('the_excerpt', 'wpautop');


if (function_exists('add_theme_support')) {
    // Add Menu Support
    add_theme_support('menus');

    // Add Thumbnail Theme Support
    add_theme_support('post-thumbnails');
    add_image_size('large', 700, '', true); // Large Thumbnail
    add_image_size('medium', 250, '', true); // Medium Thumbnail
    add_image_size('small', 120, '', true); // Small Thumbnail
    add_image_size('thumb-normal', 350, 320, true);
    add_image_size('thumb-small', 100, 100, true);
}


// If Dynamic Sidebar Exists
if (function_exists('register_sidebar')) {

    register_sidebar(array(
        'name' => __('Language', 'html5blank'),
        'description' => __('Description for this widget-area...', 'html5blank'),
        'id' => 'language',
        'before_widget' => '<div id="%1$s" class="%2$s" class="language">',
        'after_widget' => '</div>',
        'before_title' => '<div  style="display:none"><h4>',
        'after_title' => '</h4></div>'
    ));
}


/**
 * 
 */
function category($categories)
{
    $item = '';
    foreach ($categories as $category) {
        if ($category->term_id != 1) {
            $item = $category->name;
        }
    }

    return $item;
}

/** Calculate the estimated reading time for a given piece of $content.
 *
 * @param string $content Content to calculate read time for.
 * @param int $wpm Estimated words per minute of reader.
 *
 * @returns	int $time Estimated reading time.
 */
function estimated_reading_time($content = '', $wpm = 250)
{
    $clean_content = strip_shortcodes($content);
    $clean_content = strip_tags($clean_content);
    $word_count = str_word_count($clean_content);
    $time = ceil($word_count / $wpm);
    return $time;
}
