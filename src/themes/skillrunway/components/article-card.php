<?php

global $post;

setup_postdata($args['post']);

$categories = get_the_category();

if (get_the_post_thumbnail_url($post->ID, 'post-thumbnail')) {
    $image_normal  =  get_the_post_thumbnail_url($post->ID, 'thumb-normal');
    $image_small  =  get_the_post_thumbnail_url($post->ID, 'thumb-small');
} else {
    $image_normal = '';
    $image_small = '';
}

?>

<?php if ($args['type'] == 'small') : ?>
    <div class="article article--small">
        <div class="article__image article__image--small">
            <div class="article__image--inner" style="background-image: url('<?php echo $image_small; ?>');"></div>
        </div>
        <div class="article__content article__content--small">
            <h4> <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a> </h4>
            <p>
                <span> <?php echo get_the_date('j F Y'); ?> ~ <?php echo estimated_reading_time(get_the_content()); ?> min read</span>
            </p>
        </div>
    </div>
<?php endif; ?>

<?php if ($args['type'] == 'medium') : ?>
    <div class="article article--medium">
        <div class="article__image article__image--medium">
            <div class="article__image--inner" style="background-image: url('<?php echo $image_small; ?>');"></div>
        </div>
        <div class="article__content article__content--medium">
            <h4> <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a> </h4>
            <p>
                <span> <?php echo get_the_date('M j'); ?> ~ <?php echo estimated_reading_time(get_the_content()); ?> min read</span>
            </p>
        </div>
    </div>
<?php endif; ?>

<?php if ($args['type'] == 'large') : ?>
    <div class="article article--large">
        <div class="article__icon">
            <div class="article__icon--inner" style="background-image: url('<?php echo $image_small; ?>');"></div>
        </div>
        <div class="article__content article__content--large">
            <p>
                Featured in
                <a href=""><?php //echo category($categories) 
                            ?></a>
            </p>
            <h4> <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a> </h4>
            <p class="excerpt">
                <?php the_excerpt(); ?>
            </p>
            <p>
                <span> <?php echo get_the_date('M j'); ?> ~ <?php echo estimated_reading_time(get_the_content()); ?> min read</span>
            </p>
        </div>
        <div class="article__image article__image--large">
            <div class="article__image--inner" style="background-image: url('<?php echo $image_normal; ?>');"></div>
        </div>
    </div>
<?php endif; ?>