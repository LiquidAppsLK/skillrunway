<?php get_header(); ?>

<?php while (have_posts()) : the_post(); ?>
    <section class="cover--other__wrap">
        <div class="cover--other__wrap--inner d-flex align-items-center">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-10 col-lg-8 ">
                        <div class="page--title">
                            <h1>
                                <?php the_title(); ?>
                            </h1>
                        </div>
                    </div>
                </div>
            </div>
            <div class="page--meta">
                <div class="container">
                    <div class="d-flex justify-content-between align-items-center">
                        <div class="quicklinks">
                            <h4>Quick links</h4>
                            <ul>
                                <?php
                                $pages = get_pages();
                                foreach ($pages as $page) :
                                ?>
                                    <li><a href="<?php echo get_page_link($page->ID); ?>"><?php echo $page->post_title; ?></a></li>
                                <?php
                                endforeach;
                                wp_reset_postdata();
                                ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
        <section class="content__wrap">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-10 col-lg-8 ">
                        <div class="content__wrap--post__content">
                            <?php the_content(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <?php endwhile; ?>
    <?php get_footer(); ?>